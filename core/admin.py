from django.contrib import admin

from core.models.competency import Competency
from core.models.competencyType import CompetencyType
from core.models.competencyLevel import CompetencyLevel
from core.models.position import Position
from core.models.positionType import PositionType
from core.models.positionLevel import PositionLevel
from core.models.educationLevel import EducationLevel
from core.models.project import Project
from core.models.employee import Employee

admin.site.register(Position)
admin.site.register(Competency)
admin.site.register(Employee)
admin.site.register(CompetencyType)
admin.site.register(CompetencyLevel)
admin.site.register(PositionType)
admin.site.register(PositionLevel)
admin.site.register(EducationLevel)
admin.site.register(Project)

# Register your models here.
