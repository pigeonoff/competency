from django.shortcuts import render

from django.contrib.auth.models import User

from rest_framework import generics, permissions, renderers, viewsets
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse

from api.serializers.userSerializer import UserSerializer
from api.serializers.competencySerializer import CompetencySerializer
from api.serializers.competencyTypeSerializer import CompetencyTypeSerializer
from api.serializers.competencyLevelSerializer import CompetencyLevelSerializer
from api.serializers.positionSerializer import PositionSerializer
from api.serializers.positionTypeSerializer import PositionTypeSerializer
from api.serializers.positionLevelSerializer import PositionLevelSerializer
from api.serializers.educationLevelSerializer import EducationLevelSerializer
from api.serializers.projectSerializer import ProjectSerializer
from api.serializers.employeeSerializer import EmployeeSerializer

from core.models.competency import Competency
from core.models.competencyType import CompetencyType
from core.models.competencyLevel import CompetencyLevel
from core.models.position import Position
from core.models.positionType import PositionType
from core.models.positionLevel import PositionLevel
from core.models.educationLevel import EducationLevel
from core.models.project import Project
from core.models.employee import Employee

@api_view(['GET'])
def api_root(request, format=None):
    return Response({
        'users': reverse('user-list', request=request, format=format),
		'competencies': reverse('competency-list', request=request, format=format)
    })

class CompetencyTypeViewSet(viewsets.ModelViewSet):
    queryset = CompetencyType.objects.all()
    serializer_class = CompetencyTypeSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

class CompetencyLevelViewSet(viewsets.ModelViewSet):
    queryset = CompetencyLevel.objects.all()
    serializer_class = CompetencyLevelSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

class PositionTypeViewSet(viewsets.ModelViewSet):
    queryset = PositionType.objects.all()
    serializer_class = PositionTypeSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

class PositionLevelViewSet(viewsets.ModelViewSet):
    queryset = PositionLevel.objects.all()
    serializer_class = PositionLevelSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
	
class EducationLevelViewSet(viewsets.ModelViewSet):
    queryset = EducationLevel.objects.all()
    serializer_class = EducationLevelSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

class CompetencyViewSet(viewsets.ModelViewSet):
    queryset = Competency.objects.all()
    serializer_class = CompetencySerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

class PositionViewSet(viewsets.ModelViewSet):
    queryset = Position.objects.all()
    serializer_class = PositionSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

class EmployeeViewSet(viewsets.ModelViewSet):
    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

class ProjectViewSet(viewsets.ModelViewSet):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
	
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

# Create your views here.
