from django.db import models

class EducationLevel(models.Model):
    level = models.IntegerField(default = 1, primary_key = True)
    description = models.CharField(max_length = 30)
    def __str__(self):
        return(str(self.level))