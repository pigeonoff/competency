from django.db import models
		
class PositionType(models.Model):
	abbreviation = models.CharField(max_length = 3, primary_key = True)
	description = models.CharField(max_length = 30)
	def __str__(self):
	    return(self.abbreviation)
	class Meta:
	    ordering = ['abbreviation']