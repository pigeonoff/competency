from django.db import models

class Project(models.Model): # Модель проектов
	title = models.CharField(max_length = 30)
	complexity = models.IntegerField()
	start_date = models.DateField()
	deadline_date = models.DateField()
	def __str__(self):
		return (self.title + ", deadline - " + str(self.deadline))
	class Meta:
	    ordering = ['deadline_date']