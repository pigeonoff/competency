from django.db import models

from .position import Position
from .competency import Competency
from .educationLevel import EducationLevel

class Employee(models.Model): # Модель сотрудников
	name = models.CharField(max_length = 30, db_index = True)
	age = models.IntegerField()
	edLevel = models.ForeignKey(EducationLevel, default = 1, on_delete = models.CASCADE, db_index = True) # Уровень образования	
	position = models.ForeignKey(Position, related_name = 'employees', blank = True, null = True, on_delete = models.SET('unemployed')) # Занимаемая должность
	competencies = models.ManyToManyField(Competency, blank = True, null = True)
	def __str__(self):
		return (self.name + ", " + str(self.age))
	class Meta:
	    ordering = ['name']