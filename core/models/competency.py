from django.db import models

from .competencyType import CompetencyType
from .competencyLevel import CompetencyLevel

class Competency(models.Model): # Модель компетенций сотрудников
	name = models.CharField(max_length = 30, db_index = True)
	description = models.CharField(max_length = 200)
	type = models.ForeignKey(CompetencyType, default = 'No type', on_delete = models.PROTECT, db_index = True) # Тип компетенции
	level = models.ForeignKey(CompetencyLevel, default = 1, on_delete = models.CASCADE, db_index = True) # Уровень компетенции
	
	class Meta:
		verbose_name_plural = 'competencies'
		ordering = ['level']
	
	def __str__(self):
		return (self.name + " " + str(self.level))	