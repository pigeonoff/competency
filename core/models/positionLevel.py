from django.db import models

from .positionType import PositionType

class PositionLevel(models.Model):
	type = models.ForeignKey(PositionType, related_name = 'levels', null = True, on_delete = models.CASCADE)
	level = models.IntegerField(default = 1, primary_key = True)
	description = models.CharField(max_length = 30)
	def __str__(self):
	    return(str(self.level))
	class Meta:
	    ordering = ['level']