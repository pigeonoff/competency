from django.db import models

from .positionType import PositionType
from .positionLevel import PositionLevel
from .competency import Competency

class Position(models.Model): # Модель должностей
	type = models.ForeignKey(PositionType, default = 'No Type', on_delete = models.CASCADE, db_index = True) # Тип должности
	level = models.ForeignKey(PositionLevel, default = 1, on_delete = models.CASCADE, db_index = True) # Уровень должности
	required_competencies = models.ManyToManyField(Competency)
	def __str__(self):
		return (self.type + " " + str(self.level))
	class Meta:
	    ordering = ['level']