from django.conf.urls import url, include

from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework.routers import DefaultRouter, SimpleRouter
from rest_framework import renderers

from core.views import (api_root, UserViewSet, CompetencyTypeViewSet, CompetencyLevelViewSet, PositionTypeViewSet,
        PositionLevelViewSet, EducationLevelViewSet, CompetencyViewSet, PositionViewSet, EmployeeViewSet, ProjectViewSet)


competencyType_list = CompetencyTypeViewSet.as_view({
    'get': 'list',
    'post': 'create'
})
competencyType_detail = CompetencyTypeViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
})

competencyLevel_list = CompetencyLevelViewSet.as_view({
    'get': 'list',
    'post': 'create'
})
competencyLevel_detail = CompetencyLevelViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
})

positionType_list = PositionTypeViewSet.as_view({
    'get': 'list',
    'post': 'create'
})
positionType_detail = PositionTypeViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
})

positionLevel_list = PositionLevelViewSet.as_view({
    'get': 'list',
    'post': 'create'
})
positionLevel_detail = PositionLevelViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
})

educationLevel_list = EducationLevelViewSet.as_view({
    'get': 'list',
    'post': 'create'
})

educationLevel_detail = EducationLevelViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
})

competency_list = CompetencyViewSet.as_view({
    'get': 'list',
    'post': 'create'
})
competency_detail = CompetencyViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
})

position_list = PositionViewSet.as_view({
    'get': 'list',
    'post': 'create'
})
position_detail = PositionViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
})

employee_list = EmployeeViewSet.as_view({
    'get': 'list',
    'post': 'create'
})
employee_detail = EmployeeViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
})

project_list = ProjectViewSet.as_view({
    'get': 'list',
    'post': 'create'
})
project_detail = ProjectViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
})

user_list = UserViewSet.as_view({
    'get': 'list'
})
user_detail = UserViewSet.as_view({
    'get': 'retrieve'
})

# Create a router and register our viewsets with it.
router = SimpleRouter()
router.register(r'position_types', PositionTypeViewSet)
router.register(r'position_levels', PositionLevelViewSet)
router.register(r'competency_types', CompetencyTypeViewSet)
router.register(r'competency_levels', CompetencyLevelViewSet)
router.register(r'education_levels', EducationLevelViewSet)
router.register(r'competencies', CompetencyViewSet)
router.register(r'positions', PositionViewSet)
router.register(r'employees', EmployeeViewSet)
router.register(r'projects', ProjectViewSet)
router.register(r'users', UserViewSet)

# The API URLs are now determined automatically by the router.
urlpatterns = [
    url(r'^', include(router.urls))
]