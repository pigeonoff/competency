from rest_framework import serializers
from core.models.positionLevel import PositionLevel

class PositionLevelSerializer(serializers.ModelSerializer):
	
	class Meta:
		model = PositionLevel
		fields = ('type', 'level', 'description')