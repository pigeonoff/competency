from rest_framework import serializers
from core.models.competencyType import CompetencyType

class CompetencyTypeSerializer(serializers.ModelSerializer):
	levels = serializers.StringRelatedField(
		many = True,
	)
	class Meta:
		model = CompetencyType
		fields = ('abbreviation', 'description', 'levels')