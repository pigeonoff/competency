from rest_framework import serializers
from core.models.employee import Employee

class EmployeeSerializer(serializers.ModelSerializer):
	
	class Meta:
		model = Employee
		fields = ('name', 'age', 'edLevel', 'position', 'competencies')