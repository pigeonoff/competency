from rest_framework import serializers
from core.models.project import Project

class ProjectSerializer(serializers.ModelSerializer):
	
	class Meta:
		model = Project
		fields = ('title', 'complexity', 'start_date', 'deadline_date')