from rest_framework import serializers
from core.models.competency import Competency

class CompetencySerializer(serializers.ModelSerializer):
	positions = serializers.StringRelatedField(
	    many = True,
	)
	class Meta:
		model = Competency
		fields = ('name', 'description', 'type', 'level', 'positions')