from rest_framework import serializers
from core.models.competencyLevel import CompetencyLevel

class CompetencyLevelSerializer(serializers.ModelSerializer):
	
	class Meta:
		model = CompetencyLevel
		fields = ('type', 'level', 'description')