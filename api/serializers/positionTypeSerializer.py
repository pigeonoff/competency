from rest_framework import serializers
from core.models.positionType import PositionType

class PositionTypeSerializer(serializers.ModelSerializer):
	levels = serializers.StringRelatedField(
		many = True,
	)
	class Meta:
		model = PositionType
		fields = ('abbreviation', 'description', 'levels')