from rest_framework import serializers
from core.models.position import Position

class PositionSerializer(serializers.ModelSerializer):
	required_competencies = serializers.StringRelatedField(
		many = True,
	)
	class Meta:
		model = Position
		fields = ('type' ,'level', 'required_competencies')