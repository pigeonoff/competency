from rest_framework import serializers
from core.models.educationLevel import EducationLevel

class EducationLevelSerializer(serializers.ModelSerializer):

    class Meta:
	    model = EducationLevel
	    fields = ('level', 'description')